const moment = require('moment')
const TutorialSchema =require('../models/tutorialSchema')

exports.getTutorials = async(req,res)=> {
    const allTutorials = await TutorialSchema.find();

    res.status(200).send(allTutorials)
}

exports.postTutorials= async(req, res) => {
    const {title, description, published} = req.body;

    if(!title || !description ){
        return res.status(401).send("Kindly, ensure all fields are filled")
    }

    try{
    
        const new_tut = new TutorialSchema({
            title, description, published
        })
        const saved = await new_tut.save();
    
        if(saved) {
            res.status(200).send(new_tut)
        }

    }catch(err){
        res.status(401).json(err)
    }

}

exports.getSingleTutorial = async(req, res)=> {
    const {id} = req.params;
    const individualTutorial = await TutorialSchema.findOne({_id: id})
    res.status(200).send(individualTutorial)
}

exports.updateTutorial = async(req, res)=> {
    const {id} = req.params;
    const {title, description, published} = req.body 

    try{
        const updatedTutorial = await TutorialSchema.findByIdAndUpdate({_id : id}, {
            title, description, published
        }, {
            new: true
        });

        const saved = await updatedTutorial.save();

        if(saved) {
            res.status(200).send(updatedTutorial)
        }

    }catch(err){
        res.status(401).json(err)
    }
}

exports.deleteIndividualTutorials = async(req, res)=> {
    const {id} = req.params;
    try{
        const deletedTutorial = await TutorialSchema.findByIdAndDelete({_id: id})
        
        res.status(200).send(deletedTutorial)
    }catch(err){
        res.status(401).json(err)
    }
}

exports.deleteAllTutorials = async(req, res)=> {
    try{
        const deleteAllTutorial = await TutorialSchema.deleteMany()

        res.status(200).send(deleteAllTutorial)
    }catch(err){
        res.status(401).json(err)
    }
}

exports.getFilteredTutorials = async(req, res)=> {

    try{
        const {title}= req.query
        const query = {
            title : {$regex : title, $options: "i"}
        }

        const tutorial_data= await TutorialSchema.find(query)

        res.status(200).send(tutorial_data)
    }catch(err){
        res.status(401).json(err)
    }
    
}