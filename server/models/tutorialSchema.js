const mongoose = require('mongoose')

const tutSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        trime: true
    },
    description: {
        type: String,
        required: true,
        trime: true
    },
    published :{
        type: Boolean
    }
})

const TutorialSchema = new mongoose.model("tutorialSchema", tutSchema);
module.exports = TutorialSchema