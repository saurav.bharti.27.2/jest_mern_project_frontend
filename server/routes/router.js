const express  = require('express')
const router = express.Router()
const controllers = require('../controllers/UsersControllers')

router.get('/tutorials', controllers.getTutorials)

router.post('/tutorials/post', controllers.postTutorials)

router.get('/tutorial/:id', controllers.getSingleTutorial)

router.put('/tutorials/update/:id', controllers.updateTutorial)

router.delete('/tutorials/delete/:id', controllers.deleteIndividualTutorials)

router.delete('/tutorials/deleteAll', controllers.deleteAllTutorials)

router.get('/tutorials/filter', controllers.getFilteredTutorials)

module.exports = router