require("dotenv").config();
const express= require('express')

const PORT = process.env.PORT || 3000;
const app = express()

const cors = require('cors')
require('./db/connection.js')

require('./routes/router.js')
const router = require('./routes/router.js')

app.use(express.json());
app.use(cors())
app.use(router)

app.listen(PORT, ()=> {
    console.log(`Listening on port ${PORT}`)
})