import http from "../http-common";

class TutorialDataService {
  getAll() {
    return http.get("/tutorials");
  }

  get(id) {
    return http.get(`/tutorial/${id}`);
  }

  create(data) {
    return http.post("/tutorials/post", data);
  }

  update(id, data) {
    return http.put(`/tutorials/update/${id}`, data);
  }

  delete(id) {
    return http.delete(`/tutorials/delete/${id}`);
  }

  deleteAll() {
    return http.delete(`/tutorials/deleteAll`);
  }

  findByTitle(title) {
    return http.get(`/tutorials/filter?title=${title}`);
  }
}

export default new TutorialDataService();