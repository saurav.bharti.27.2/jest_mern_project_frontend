import React from 'react';
import TutorialsList from '../components/tutorials-list.component';
import { render, fireEvent, waitFor, screen, getByRole} from '@testing-library/react';
import TutorialDataService from '../services/tutorial.service'
import userEvent from '@testing-library/user-event';
import {BrowserRouter} from 'react-router-dom'

beforeAll(()=>{
    jest.spyOn(console, 'error').mockImplementation(()=> {})
})
afterAll(()=> {
    console.error.mockRestore()
})

afterEach(()=> {
    jest.clearAllMocks()
})
const dummyData = {
    data : [
        {
            "_id": 1,
            "title": "Yellow",
            "description": "Learn something today",
            "published" : true
        },
        {
            "_id": 2,
            "title": "Blue",
            "description": "Become better everyday",
            "published" : false
        }
    ]
}

test("testing the rendering of our tutorial list page", async()=> {
    const mockTutorialDataService = jest.spyOn(TutorialDataService, 'findByTitle')
    mockTutorialDataService.mockResolvedValue(dummyData)
    
    const {getByText, getByPlaceholderText, debug} = render(
        <TutorialsList />
    )
    const search = getByText(/search/i)
    const input = getByPlaceholderText(/search by title/i)

    expect(getByText(/search/i)).toBeInTheDocument()

    fireEvent.change(input, {target: {value: 'Trello'}})

    expect(search).toHaveAttribute('type', 'button')

    fireEvent.click(search)
    
    await waitFor(()=> {

        expect(mockTutorialDataService).toHaveBeenCalledWith('Trello')
        expect(mockTutorialDataService).toHaveBeenCalledTimes(1)
    })
    

})

// mocking the function call so that we get the tutorial of our choice

test("mocking the function call so that we get the tutorial of our choice", async()=> {

    const mockTutorialDataService = jest.spyOn(TutorialDataService, 'getAll')
    mockTutorialDataService.mockResolvedValue(dummyData)

    const {getByText, rerender, debug, getByLabelText, getByRole} = render(
        <BrowserRouter>
            <TutorialsList/>
        </BrowserRouter>
    )

    await waitFor(()=> {
        expect(getByText(/yellow/i)).toBeInTheDocument()
    })
    
    const selectBox = getByText(/yellow/i)
    
    fireEvent.click(selectBox);

    rerender(
        <BrowserRouter>
            <TutorialsList/>
        </BrowserRouter>
    )
    await waitFor(()=> {
        expect(getByText(/something/i)).toBeInTheDocument()
    })

    
    // debug();
})



